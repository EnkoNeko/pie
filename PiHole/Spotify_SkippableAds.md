So Spotify is testing skippable ads in some areas, and they're messing with the ad blocking. Whatever they've done with the latest updates means if the ad can't play (is ad-blocked), nothing plays - it just stays at the start of the ad.



You could try whitelisting `adeventtracker.spotify.com` (and ofc letting ads in)


Also see:  
https://community.spotify.com/t5/Desktop-Windows/Disable-auto-update-Choose-where-Spotify-installs-This-is/m-p/2140036#M22468  
https://old.reddit.com/r/Piracy/comments/aumaqg/guide_block_spotify_ads_on_windows_working/  
https://old.reddit.com/r/Piracy/comments/9jvlf8/get_rid_of_spotify_adsbannerslimited_skips_and/  
https://old.reddit.com/r/Piracy/comments/a29axi/the_definitive_spotify_adblock_on_windows/  